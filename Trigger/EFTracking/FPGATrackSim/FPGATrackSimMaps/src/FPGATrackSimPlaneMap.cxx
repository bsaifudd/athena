// Copyright (C) 2002-2023 CERN for the benefit of the ATLAS collaboration


/**
 * @file FPGATrackSimPlaneMap.h
 * @author Riley Xu - rixu@cern.ch (rewrite from FTK)
 * @date Janurary 7th, 2020
 * @brief See header.
 */


#include "FPGATrackSimMaps/FPGATrackSimPlaneMap.h"
#include "FPGATrackSimMaps/Remappings.h"
#include <AsgMessaging/MessageCheck.h>
#include <numeric>
#include <sstream>
#include <utility>

using namespace asg::msgUserCode;
using namespace std;

///////////////////////////////////////////////////////////////////////////////
// Constructor/Desctructor
///////////////////////////////////////////////////////////////////////////////

FPGATrackSimPlaneMap::FPGATrackSimPlaneMap(const std::string & filepath, unsigned region, unsigned stage, std::vector<int> layerOverrides) :
    m_map(static_cast<int>(SiliconTech::nTechs),
          vector<vector<LayerSection>>(static_cast<int>(DetectorZone::nZones))
    ),
    m_layerOverrides(std::move(layerOverrides))
{
    ifstream fin(filepath);
    if (!fin.is_open())
    {
        ANA_MSG_FATAL("Couldn't open " << filepath);
        throw ("FPGATrackSimPlaneMap Couldn't open " + filepath);
    }
    // Reads the header of the file to resize all the vector members
    allocateMap(fin, stage);

    // Seek to the correct region
    seek(fin, region);

    // Reads the rest of the file to populate all the member vectors
    readLayers(fin, stage);

    // Postprocessing on coordinate indices
    for (uint32_t l = 0; l < m_nLogiLayers; l++)
    {
        m_coordOffset[l] = m_nCoords;
        m_nCoords += m_dimension[l];
        for (uint32_t i = 0; i < m_dimension[l]; i++)
            m_coordLayer.push_back(l);
    }

    ANA_MSG_INFO("Using " << m_nLogiLayers << " logical layers and " << m_nCoords << " coordinates");
}

FPGATrackSimPlaneMap::FPGATrackSimPlaneMap(std::ifstream& fin, unsigned region, unsigned stage, std::vector<int> layerOverrides) :
    m_map(static_cast<int>(SiliconTech::nTechs),
          vector<vector<LayerSection>>(static_cast<int>(DetectorZone::nZones))
    ),
    m_layerOverrides(std::move(layerOverrides))
{
    // Reads the header of the file to resize all the vector members
    allocateMap(fin, stage);

    // Seek to the correct region
    seek(fin, region);

    // Reads the rest of the file to populate all the member vectors
    readLayers(fin, stage);

    // Postprocessing on coordinate indices
    for (uint32_t l = 0; l < m_nLogiLayers; l++)
    {
        m_coordOffset[l] = m_nCoords;
        m_nCoords += m_dimension[l];
        for (uint32_t i = 0; i < m_dimension[l]; i++)
            m_coordLayer.push_back(l);
    }

    ANA_MSG_DEBUG("Using " << m_nLogiLayers << " logical layers and " << m_nCoords << " coordinates");
}
// Reads the header of the file to resize all the vector members
void FPGATrackSimPlaneMap::allocateMap(ifstream & fin, uint32_t stage)
{
    // Initialize read variables
    vector<int> layerCounts((int)SiliconTech::nTechs * static_cast<int>(DetectorZone::nZones)); // pixel_barrel, pixel_EC, SCT_barrel, SCT_EC
    std::string line, silicon, detReg, layerKey, geoKey;
    bool ok = true;
    // Read Geometry Version
    ok = ok && getline(fin, line);
    while (line.empty() || line[0] == '!')
    {
        ok = ok && getline(fin, line);
    }
    ANA_MSG_VERBOSE(line);
    istringstream sline(line);
    ok = ok && (sline >> geoKey);
    m_diskIndex = Remappings::diskIndices(geoKey);

    ANA_MSG_DEBUG("Allocating map for geometry " << geoKey <<" diskIndex size="<<m_diskIndex.size());
    m_moduleRelabel = std::make_unique<FPGATrackSimModuleRelabel>(geoKey, false);



    // Read number of logical layers
    if (stage == 1)
    {
        ok = ok && getline(fin, line);
        ANA_MSG_VERBOSE(line);
        istringstream sline(line);
        ok = ok && (sline >> m_nLogiLayers >> layerKey);
        ok = ok && (layerKey == "logical_s1");
        ok = ok && getline(fin, line); // skip the 2nd stage line
    }
    else if (stage == 2)
    {
        ok = ok && getline(fin, line); // skip the 1st stage line
        ok = ok && getline(fin, line);
        ANA_MSG_VERBOSE(line);
        istringstream sline(line);
        ok = ok && (sline >> m_nLogiLayers >> layerKey);
        ok = ok && (layerKey == "logical_s2");
    }
    else ANA_MSG_ERROR("Bad stage " << stage << ". Stage should be 1 or 2");

    if (!m_layerOverrides.empty()) m_nLogiLayers = m_layerOverrides.size();

    // Read number of physical layers
    int nHeaderLines = 6;
    for (int i = 0; i < nHeaderLines; i++)
    {
        if (!ok) break;
        ok = ok && getline(fin, line);
        ANA_MSG_VERBOSE(line);

        istringstream sline(line);
        ok = ok && (sline >> layerCounts[i] >> silicon >> detReg);

        ok = ok && ( (i < (nHeaderLines/2) && silicon == "pixel") || (i >= (nHeaderLines/2) && silicon == "SCT") );
        ok = ok && ( (i % (nHeaderLines/2) == 0 && detReg == "barrel") || (i % (nHeaderLines/2) != 0 && (detReg == "endcap+" || detReg == "endcap-")) );
    }
    
    if (!ok) ANA_MSG_FATAL("Error reading layer counts");
    m_nDetLayers = std::accumulate(layerCounts.begin(), layerCounts.end(), 0);

    // Resize objects
    m_map[(int)SiliconTech::pixel][(int)DetectorZone::barrel].resize(layerCounts[0]);
    m_map[(int)SiliconTech::pixel][(int)DetectorZone::posEndcap].resize(layerCounts[1]);
    m_map[(int)SiliconTech::pixel][(int)DetectorZone::negEndcap].resize(layerCounts[2]);
    m_map[(int)SiliconTech::strip][(int)DetectorZone::barrel].resize(layerCounts[3]);
    m_map[(int)SiliconTech::strip][(int)DetectorZone::posEndcap].resize(layerCounts[4]);
    m_map[(int)SiliconTech::strip][(int)DetectorZone::negEndcap].resize(layerCounts[5]);

    m_dimension.resize(m_nLogiLayers);
    m_layerInfo.resize(m_nLogiLayers);
    m_coordOffset.resize(m_nLogiLayers);
}

// Seeks to the selected region
void FPGATrackSimPlaneMap::seek(ifstream & fin, unsigned region)
{
    std::string line, key;
    unsigned region_read = -1;

    while (getline(fin, line))
    {
        if (line.empty() || line[0] == '!') continue;
        ANA_MSG_VERBOSE(line);
        istringstream sline(line);

        bool ok = static_cast<bool>(sline >> key >> region_read);
        if (ok && key == "region" && region_read == region) break;
    }

    ANA_MSG_DEBUG("seek() ended at:" << line);
}


// Reads the rest of the file to populate all the member vectors
void FPGATrackSimPlaneMap::readLayers(ifstream & fin, uint32_t stage)
{
    // Initialize read variables
    int BEC{}, physLayer{}, physDisk{}, logiLayer{}, logiLayer1{}, logiLayer2{}, stereo{}, nDim{};
    string line, silicon, planeKey1, planeKey2, stereoKey;
    bool ok = true;

    uint32_t linesRead = 0;

    // Each line is a detector layer
    while (getline(fin, line))
    {
        if (line.empty() || line[0] == '!') continue;
        ANA_MSG_VERBOSE(line);
        istringstream sline(line);

        ok = ok && (sline >> silicon >> BEC >> physDisk >> physLayer);

        // Detector
        DetectorZone zone  = DetectorZone::undefined;
        if (BEC == 0) zone = DetectorZone::barrel;
        else if (BEC == 1) zone = DetectorZone::posEndcap;
        else if (BEC == 2) zone = DetectorZone::negEndcap;
        else ANA_MSG_FATAL("Bad Detector zone" << BEC);

        // Case on silicon type
        int sil = 0;
        SiliconTech siTech = SiliconTech::pixel;
        if (silicon == "pixel")
        {
            sil = static_cast<int>(SiliconTech::pixel);
            nDim = 2;
            stereo = -1;
            ok = ok && (sline >> planeKey1 >> logiLayer1 >> planeKey2 >> logiLayer2);
            ok = ok && (planeKey1 == "plane1" && planeKey2 == "plane2");
        }
        else if (silicon == "SCT")
        {
            sil = static_cast<int>(SiliconTech::strip);
            siTech = SiliconTech::strip;

            nDim = 1;
            ok = ok && (sline >> stereoKey >> stereo >> planeKey1 >> logiLayer1 >> planeKey2 >> logiLayer2);
            ok = ok && (planeKey1 == "plane1" && planeKey2 == "plane2" && stereoKey == "stereo");
        }
        else ANA_MSG_FATAL("Bad silicon type " << silicon);


        if (!ok) break;

        // Logical layer
        if (!m_layerOverrides.empty()) logiLayer = getOverrideLayer(siTech, zone, physLayer);
        else if (stage == 1) logiLayer = logiLayer1;
        else if (stage == 2) logiLayer = logiLayer2;

        // Check number of layers
        linesRead++;
        if (logiLayer >= (int)m_nLogiLayers) ANA_MSG_FATAL("Logical layer " << logiLayer << " exceeds expected number " << m_nLogiLayers);

        // Store to map
        m_map.at(sil).at(BEC).at(physLayer).layer = logiLayer;
        if (logiLayer >= 0)
        {
            m_dimension[logiLayer] = nDim;
            m_map[sil][BEC][physLayer].section = m_layerInfo[logiLayer].size(); // i.e. index into m_layerInfo[logiLayer] entry below
            m_layerInfo[logiLayer].push_back({ siTech, zone, physLayer, physDisk, stereo});
        }
        if (m_nDetLayers == linesRead) break;
    }

    if (!ok) ANA_MSG_FATAL("Error reading file");
}


///////////////////////////////////////////////////////////////////////////////
// Interface Functions
///////////////////////////////////////////////////////////////////////////////


void FPGATrackSimPlaneMap::map(FPGATrackSimHit & hit) const
{
    //TODO WW check of isMapped commented out this might work?
    
    // re-assign layers in the pixel endcap to be each individual disk
    // technically this returns a success/fail but I'm not sure we need it?
    // This should only happen if the hit is not already mapped.
    if (!hit.isMapped()){
        if(!hit.isRemapped()){
            m_moduleRelabel->remap(hit);
        }
    }
    const LayerSection &pinfo = getLayerSection(hit.getDetType(), hit.getDetectorZone(), hit.getPhysLayer());
    hit.setSection(pinfo.section);
    hit.setLayer(pinfo.layer);
    if (!hit.isMapped()) // failsafe if for some reason someone calls this on a clustered hit again, or something
        hit.setHitType(HitType::mapped);

    // Special case if this is a spacepoint (now possible because slicing engine happens after SP formation).
    if (hit.getHitType() == HitType::spacepoint) {
        const LayerSection &pinfo_sp = getLayerSection(hit.getPairedDetType(), hit.getPairedDetZone(), hit.getPairedPhysLayer());
        hit.setPairedSection(pinfo_sp.section);
        hit.setPairedLayer(pinfo_sp.layer);
    }
}


///////////////////////////////////////////////////////////////////////////////
// Helper Functions
///////////////////////////////////////////////////////////////////////////////

// Gets the logical layer number based on the override list
int FPGATrackSimPlaneMap::getOverrideLayer(SiliconTech si, DetectorZone dz, int physLayer) // TODO does dz need edit for negEndcap?
{
    int code = static_cast<int>(si) * 1000 + static_cast<int>(dz) * 100 + physLayer;
    for (unsigned i = 0; i < m_layerOverrides.size(); i++)
        if (code == m_layerOverrides[i]) return i;
    return -1;
}

std::string FPGATrackSimPlaneMap::layerName(uint32_t layer, uint32_t section) const
{
    std::string out;
    out += (isPixel(layer) ? "PIX" : "SCT");
    out += (isEC(layer, section) ? "_EC_" : "_B_");
    out += std::to_string(getLayerInfo(layer, section).physLayer);
    return out;
}
